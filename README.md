# README #

* Please read the following short notes before playing the game.

### What is the name and version of the game? ###

* Math Bond Game to 10
* 1.0.0.1

### About ###

* This is a game dedicated to children who would like to improve their Maths skills.
* A learner will first click on the start button when they're ready to play the game.
* For every correct answer, points will be given based on the number of seconds taken to answer.
* When the learner is done, they click on the Finish button.
* A report with a score, number of seconds taken, and corrections if any, will be printed on a popup.
* The learner can then decide to restart the game and retry.

### Test data ###

* What is 2 + 3 ?
* If answer is 5, number of seconds before pressing a key is less than 3, points will be 10.
* else if answer is wrong, 10 points will be deducted. If points are less than 0, then points will be 0.

### Who do I talk to? ###

* Retshepile Mahasha E-mail: mahasha.rethepile@gmail.com
* Ryan Van Rensburg